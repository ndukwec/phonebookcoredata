//
//  Contact.swift
//  PhoneBookCoreData
//
//  Created by Chinedu Amadi-Ndukwe on 26/11/2014.
//  Copyright (c) 2014 Chinedu Amadi-Ndukwe. All rights reserved.
//

import Foundation
import CoreData

class Contact: NSManagedObject {
    @NSManaged var name : String
    @NSManaged var number :String
    
}