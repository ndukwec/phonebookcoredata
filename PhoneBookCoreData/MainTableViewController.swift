//
//  MainTableViewController.swift
//  PhoneBookCoreData
//
//  Created by Chinedu Amadi-Ndukwe on 26/11/2014.
//  Copyright (c) 2014 Chinedu Amadi-Ndukwe. All rights reserved.
//

import UIKit
import CoreData

class MainTableViewController: UITableViewController {
    
    var managedObjectContext: NSManagedObjectContext?
    var myContacts = [Contact]()
    var filteredContacts = [Contact]()

    override func viewDidLoad() {
        super.viewDidLoad()

    
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        if let context = appDelegate.managedObjectContext {
            self.managedObjectContext = context
        }
        
        pullContacts()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    @IBAction func unwindtolist(segue: UIStoryboardSegue){
    
        println("vsfbebeb")
        let source = segue.sourceViewController as NewViewController
        let name = source.contactName.text
        let number = source.contactNumber.text
        println("name: \(name) and number \(number)")
        
        let newContact:Contact = NSEntityDescription.insertNewObjectForEntityForName("Contact", inManagedObjectContext: self.managedObjectContext!) as Contact
        
        newContact.name = name
        newContact.number = number
        pullContacts()
    }

    func pullContacts(){
        myContacts = []
        let fetchRequest = NSFetchRequest(entityName: "Contact")
        if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [Contact]{
            myContacts = fetchResults
        }
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return myContacts.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as UITableViewCell

        // Configure the cell...
        var currentContact = myContacts[indexPath.row]
        cell.textLabel?.text = currentContact.name

        return cell
    }
    
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showContact" {
            println("segue ShowContact")
            if let destination = segue.destinationViewController as? SubViewController {
                if let cell = sender as? UITableViewCell {
                    if let indexPath = self.tableView.indexPathForCell(cell) {
                        destination.chosenContact = self.myContacts[indexPath.row]
                    }
                }
            }
            
            
            
//            if let indexPath = self.tableView.indexPathsForSelectedRows() {
//             //  destination.chosenContact = Contact()
//            }
        }
       
    }

}
