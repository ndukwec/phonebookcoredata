//
//  SubViewController.swift
//  PhoneBookCoreData
//
//  Created by Chinedu Amadi-Ndukwe on 26/11/2014.
//  Copyright (c) 2014 Chinedu Amadi-Ndukwe. All rights reserved.
//

import UIKit

class SubViewController: UIViewController {
    
    var chosenContact: Contact?
    
    @IBOutlet weak var numberLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberLabel.text = chosenContact?.number

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
